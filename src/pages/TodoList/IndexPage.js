import React from 'react';
import uuidv4 from 'uuid/v4';
import { Button, Input, Layout, List, Checkbox } from 'antd';
import styles from './IndexPage.css';
const { Content } = Layout;

function Todo(props) {
  return (
    <List.Item>
      <Checkbox
        className={props.status ? styles.check : ' 1'}
        checked={props.status}
        onChange={props.changeTodo}
      >
        {props.name}
      </Checkbox>

      <Button
        className={styles.btndel}
        type="danger"
        size="small"
        shape="circle"
        icon="cross"
        onClick={props.deleTo}
      />
    </List.Item>
  );
}
function Done(props) {
  return (
    <List.Item>
      <Checkbox
        className={props.status ? styles.check : ' 1'}
        checked={props.status}
        onChange={props.change}
      >
        {props.name}
      </Checkbox>
      <Button
        className={styles.btndel}
        type="danger"
        size="small"
        shape="circle"
        icon="cross"
        onClick={props.doneDelete}
      />
    </List.Item>
  );
}
class IndexPage extends React.Component {
  constructor(props, context) {
    //用来储存新增项目
    super(props, context);
    this.dispatch = props.dispatch;
    this.state = {
      todoList: [
        {
          id: uuidv4(),
          name: '草莓',
          status: false,
        },
        {
          id: uuidv4(),
          name: '苹果',
          status: false,
        },
        {
          id: uuidv4(),
          name: '葡萄',
          status: false,
        },
      ],
      doneList: [],
    };
  }
  handleClick = () => {
    var s = {
      id: uuidv4(),
      name: 'ere',
      status: false,
    };
    s.name = this.state.item;
    var lists = this.state.todoList;
    lists.push(s);
    this.setState({ todoList: lists });
  };

  render() {
    return (
      <Layout className={styles.layout}>
        <h1>TodoList</h1>
        <Content>
          <Input
            value={this.state.item}
            placeholder="请输入代办事项。"
            onChange={e => {
              this.setState({ item: e.target.value });
            }}
            onPressEnter={e => {
              if (e.target.value === '') {
                alert('内容不能为空');
              } else {
                this.handleClick();
              }
            }}
          />
          <Button
            type="primary"
            icon="plus"
            disabled={this.state.item === ''}
            onClick={this.handleClick}
          >
            新增
          </Button>
          <h2>
            TodoList <small>正在进行的事</small>
          </h2>
          <span>{this.state.todoList.length}</span>
          <List
            className={styles.list}
            bordered
            dataSource={this.state.todoList}
            renderItem={(item, index) => (
              <Todo
                name={item.name}
                status={item.status}
                deleTo={() => {
                  var lists = this.state.todoList;
                  lists.splice(index, 1);
                  this.setState({
                    todoList: lists,
                  });
                }}
                changeTodo={e => {
                  var lists = this.state.todoList;
                  lists[index].status = item.status;
                  var doneLists = lists.splice(index, 1);
                  var value = this.state.doneList.concat(doneLists);
                  this.setState({
                    todoList: lists,
                    doneList: value,
                  });
                }}
              />
            )}
          />
          <h2>
            TodoList <small>已经完成的事</small>
          </h2>
          <span>{this.state.doneList.length}</span>
          <List
            className={styles.list}
            bordered
            dataSource={this.state.doneList}
            renderItem={(item, index) => (
              <Done
                name={item.name}
                status={item.status}
                change={e => {
                  var lists = this.state.doneList;
                  lists[index].status = item.status;
                  var todoLists = lists.splice(index, 1);
                  var value = this.state.todoList.concat(todoLists);
                  this.setState({
                    todoList: value,
                    doneList: lists,
                  });
                }}
                doneDelete={() => {
                  var lists = this.state.doneList;
                  lists.splice(index, 1);
                  this.setState({
                    doneList: lists,
                  });
                }}
              />
            )}
          />
        </Content>
      </Layout>
    );
  }
}
export default IndexPage;
